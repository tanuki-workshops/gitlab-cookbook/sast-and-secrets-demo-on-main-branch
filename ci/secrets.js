const fs = require("fs")

if (fs.existsSync("./demo.secrets.js")) {
  let start = new Date()

  // ==================================
  //  Severity:
  //  Critical, High, Medium
  // ==================================
  let severity = "Critical"
  
  // ==================================
  //  Confidence:
  //  Unknown Confirmed Experimental
  // ==================================
  let confidence = "Confirmed"
  
  // ==================================
  //  Scanner's info
  // ==================================
  let scannerName = "GitLab Secrets"
  let scannerId = "gitlab_secrets"
  let scannerSrc = "https://gitlab.com"
  let scannerVendor = "GitLab"
  let scannerVersion = "1.8.2"
  
  // ==================================
  //  Vulnerabilities' list
  // ==================================
  let vulnerabilities = [
    {
      category: "secret_detection",
      name: `AWS_TOKEN`,
      message: `❌ AWS token detected`,
      description: `🖐️ AWS token detected`,
      cve: "000001",
      severity: "Critical", // Critical, High, Medium
      confidence: "Confirmed", // Unknown Confirmed Experimental
      scanner: {id: scannerId, name: scannerName},
      location: {
        file: "./demo.secrets.js", 
        start_line: 3,
        end_line: 33,
        commit: {
          author: process.env.CI_COMMIT_AUTHOR,
          message: process.env.CI_COMMIT_MESSAGE,
          date: process.env.CI_COMMIT_TIMESTAMP,
          sha: process.env.CI_COMMIT_SHA
        }
      },
      identifiers: [
        {
          type: "aws_token_rule_id",
          name: "aws token rule",
          value: "aws token"
        }
      ]
    }              
  ]
  
  let end = new Date() - start
  
  // ==================================
  //  SECRET Report
  // ==================================
  let secretReport = {
    version: "3.0",
    vulnerabilities: vulnerabilities,
    remediations: [],
    scan: {
      scanner: {
        id: scannerId,
        name: scannerName,
        url: scannerSrc,
        vendor: {
          name: scannerVendor
        },
        version: scannerVersion
      },
      type: "secret_detection",
      start_time: start,
      end_time: end,
      status: "success"
    }
  }
  
  fs.writeFileSync("./gl-secret-detection-report.json", JSON.stringify(secretReport, null, 2))
}



